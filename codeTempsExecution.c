#include <stdio.h>
#include <omp.h>

#define NB_ITER 1000000000

int main(void){

//#pragma omp parallel
//	{

	int i;
	double pi_approchee = 0.0;
	double pas =1.0/NB_ITER;
	
// Non : car dans ce cas une seule boucle, donc un seul thread
//#pragma omp parallel private(i, pi_approchee, pas)
//#pragma omp parallel private(val)

	#pragma omp parallel 
	{
	//En mettant une reduction, l'opération est directement posée comme atomic sur la variable.
		#pragma omp for reduction(+:pi_approchee) //Parrallélisation de la boucle. Sans réduction : erreur sur la valeur de pi
		//Au total, accélération par 3 de la vitesse
		for(i =0; i<NB_ITER; i++){
			double x = pas * (i-0.5);
			//#pragma omp atomic //Correction du résultat sans partage de la boucle
			pi_approchee = pi_approchee + 4.0 * pas / (1.0 + x*x);
		}
	}
	
	//#pragma omp parallel reduction(+:val)
	//pi_approchee = pi_approchee + val;
	
	printf("Valeur approchee de PiI %0.20f \n", pi_approchee);

	return 0;
}
