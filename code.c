// Code de base pour tester la lib OpenMP
//
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

int main(){

	printf("coucou\n");
	
	//Ici on est dans le thread principal

	//Ici on déclare les variables utilisées dans la zone parallèle
	//Des variables partagées
	int n;
#pragma omp parallel shared(n)
	// ! L'accès à la variable doit être protégé ! 

	//Des variables privées
	int i;
	int j;
#pragma omp parallel private(i,j)
	
	//On peut paramétrer la partie multithread
	//  #pragma omp parallel if (n>3)
	#pragma omp parallel
	{
		//Ici on est en parrallèle
	
// attendent que tous les threads s'attendent
#pragma omp barrier

		// les thread s'attendent, sauf si l'option nowait est posée
		// #pragma omp parallel nowait
	}

	//Une opération de réduction (somme ...) peut être effectuée
#pragma omp parallel reduction(+:i) //somme sur i

	//Chaque itération de la boucle est effectuée une seule fois, une par thread
	#pragma omp for
	for(int i=0;i<100;i++){
		printf("boucle %d",i);
	}

	// Portion de code qui n'est executée que par un seul thread = Section critique ,avec variable partagée par exemple
	#pragma omp critical
	{

	}	


}


