#include <omp.h>
#include <stdio.h>

int main(){
	
#pragma omp parallel
	{

	#pragma omp for
	for(int i=0; i<10 ; i++){
		printf("Handling index : %d\n", i);
		printf("Nombre de threads : %d\n", omp_get_num_threads());
		printf("Numero du thread actuel : %d\n", omp_get_thread_num());
		printf("Nombre d'unité de traitement : %d\n", omp_get_num_procs());
	}

	}
}
